function getMessages (url, container) {
  fetch(url).then(response => {
    console.log("Response received!", response)
    if (!response.ok) {
      throw new Error("HTTP error " + response.status);
    }
    return response.json();
 }).then(messages => {
    console.log(messages);
    messages_str = "<div class='message-list'>"
    messages.forEach(message => {
      message_str = '<div class="message"><p style="font-size: x-large; font-family: Arial;">'
      message_str += `<b>Autor ${message.author}, Fecha: ${formatDate(message.date)}</b></p>`
      if (message.isimg) {
        message_str += `<p><img src=${message.text} alt="Imagen no disponible" style="width: 19%; height: auto;">`
      } else {
        message_str += `<p align="center" style="font-size: x-large; font-family: Arial;">${message.text}`
      }
      message_str += "</p></div><hr class='messages-hr'>"
      messages_str += message_str
    });
    message_str += "</div>"
    container.innerHTML = messages_str;
 }).catch(function () {
    console.log("Error decoding JSON")
 })
}

function formatDate(date) {
  const options = {
    month: 'short',
    day: 'numeric',
    year: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
    hour12: true
  };

  return new Date(date).toLocaleDateString('en-US', options);
}

window.addEventListener("DOMContentLoaded", (event) => {
  console.log("DOM fully loaded and parsed");
  const container = document.querySelector('#messages');
  getMessages(url_messages, container);
  setInterval(() => {
    console.log("Interval");
    getMessages(url_messages, container);
    }, 30 * 1000);
});
