from django.test import TestCase
from django.test import Client

from .models import AccessKeys, Room

# Create your tests here.

class ClassFunctions(TestCase):
    def test_get_total_messages(self):
        room = Room(name="prueba")
        self.assertEqual(room.get_total_messages(), 0)
        room.total_messages_image = 6
        self.assertEqual(room.get_total_messages(), 6)
        # tener un total de 9
        room.total_messages_text = 3
        self.assertEqual(room.get_total_messages(), 15)

    def test_is_empty(self):
        room = Room(name="prueba")
        self.assertEqual(room.is_empty(), True)
        room.total_messages_image = 3
        self.assertEqual(room.is_empty(), False)


class ResourceTests(TestCase):
    def test_main(self):
        """Comprobamos que se realiza correctamente el GET al recurso principal"""
        c = Client()
        response = c.get('/')
        self.assertRedirects(response, '/login?redir=', status_code=302)
        clave = AccessKeys.objects.create(key="prueba")
        response = c.post('/login?redir=', {'key': clave.key})
        self.assertRedirects(response, '/', status_code=302)
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h1>Listado de Salas</h1>', content)

    def test_login(self):
        """Prueba de la página de login"""
        c = Client()
        response = c.get('/login')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2>Autentícate</h2>', content)
        clave = AccessKeys.objects.create(key="prueba")
        response = c.post('/login', {'key': clave.key})
        self.assertRedirects(response, '/', status_code=302)

    def test_help(self):
        """Prueba de la página de ayuda"""
        c = Client()
        response = c.get('/ayuda')
        self.assertRedirects(response, '/login?redir=ayuda',
                             status_code=302)
        clave = AccessKeys.objects.create(key="prueba")
        response = c.post('/login?redir=ayuda', {'key': clave.key})
        self.assertRedirects(response, '/ayuda', status_code=302)
        response = c.get('/ayuda')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h1>Ayuda</h1>', content)

    def test_configuration(self):
        """Prueba de la página de configuración"""
        c = Client()
        response = c.get('/configuracion')
        self.assertRedirects(response, '/login?redir=configuracion',
                             status_code=302)
        clave = AccessKeys.objects.create(key="prueba")
        response = c.post('/login?redir=configuracion', {'key': clave.key})
        self.assertRedirects(response, '/configuracion', status_code=302)
        response = c.get('/configuracion')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h1>Configuración</h1>', content)

    def test_room(self):
        """Prueba de la página de una sala"""
        c = Client()
        response = c.get('/azul')
        self.assertRedirects(response, '/login?redir=azul',
                             status_code=302)
        clave = AccessKeys.objects.create(key="prueba")
        response = c.post('/login?redir=azul', {'key': clave.key})
        self.assertRedirects(response, '/azul', status_code=302)
        response = c.get('/azul')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2>¡No hay ninguna conversación aún!</h2>', content)

    def test_dynamic(self):
        """Prueba de la página dinámica de una sala"""
        c = Client()
        response = c.get('/dinamica/azul')
        self.assertRedirects(response, '/login?redir=dinamica/azul', status_code=302)
        clave = AccessKeys.objects.create(key="prueba")
        response = c.post('/login?redir=dinamica/azul', {'key': clave.key})
        self.assertRedirects(response, '/dinamica/azul', status_code=302)
        # dinámica de la sala creada
        response = c.get('/dinamica/azul')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h2>Listado de mensajes</h2>', content)

    def test_json_room(self):
        """Prueba sala en formato JSON"""
        c = Client()
        response = c.get('/json/azul')
        self.assertRedirects(response, '/login?redir=json/azul', status_code=302)
        clave = AccessKeys.objects.create(key="prueba")
        response = c.post('/login?redir=json/azul', {'key': clave.key})
        self.assertRedirects(response, '/json/azul', status_code=302)
        response = c.get('/json/chuli')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('[]', content)