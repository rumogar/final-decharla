from django.shortcuts import render, redirect
from django.http import HttpResponseNotAllowed, JsonResponse
from django.template.defaulttags import register
from django.utils.datastructures import MultiValueDictKeyError
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt

from .forms import RoomForm, SpeakerForm, StyleForm
from .forms import MessageForm, AuthenticateForm

from .models import Room, Speaker, AccessKeys

from .xml_roommessages import RoomUpdateHandler
from xml.sax import make_parser
import random

#para que funcione un get de un diccionario
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

def login(request):
    form = AuthenticateForm()  # Instancio una variable de
    # tipo AuthenticateForm
    if request.method == 'POST' or request.method == 'GET':
        if request.method == 'POST':
            access_key = request.POST['key']
            try:
                AccessKeys.objects.get(key=access_key)
                # Si la access_key es correcta, creamos un nuevo usuario
                cookie_id = random.randint(1, 2 ** 32)
                last_connected = timezone.now()
                new_speaker = Speaker(cookie_id=cookie_id,
                                      last_connected=last_connected)
                new_speaker.save()
                try:
                    redir = request.GET['redir']
                    url_redir = '/' + str(redir)
                except MultiValueDictKeyError:
                    url_redir = '/'
                response = redirect(url_redir)
                response.set_cookie('cookie_id', cookie_id)
                return response
            except AccessKeys.DoesNotExist:
                context = {
                    'form': form,
                    'key_not_valid': True,
                }
                response = render(request, 'decharla/authenticate.html',
                                  context)
                response.status_code = 401
                return response
        if request.method == 'GET':
            context = {
                'form': form,
                'key_not_valid': False,
            }
            return render(request, 'decharla/authenticate.html', context)
    else:
        return HttpResponseNotAllowed(['POST', 'GET'])

def get_rooms_parameters(rooms_list):
    text_messages = 0
    image_messages = 0
    active_rooms = 0
    for room in rooms_list:
        text_messages = text_messages + room.total_messages_text
        image_messages = image_messages + room.total_messages_image
        if not room.is_empty():
            active_rooms = active_rooms + 1
    return text_messages, image_messages, active_rooms

def index(request):
    if request.method == 'GET' or request.method == 'POST':
        if 'cookie_id' in request.COOKIES:
            cookie_id = request.COOKIES['cookie_id']
        else:  # Si no hay cookie, redirigimos a página login sabiendo
            # que se ha pedido la página principal
            query_string = 'redir='
            redirect_url = '/login?' + query_string
            return redirect(redirect_url)
        speaker = Speaker.objects.get(cookie_id=cookie_id)
        if request.method == 'GET':
            # Obtenemos la lista de salas
            rooms_list = Room.objects.all()
            total_messages_text, total_messages_image, active_rooms = \
                get_rooms_parameters(rooms_list)
            room_form = RoomForm()
            # Calculamos el número de mensajes nuevos en cada sala desde
            rooms_msg_since_last = {}
            for room in rooms_list:
                rooms_msg_since_last[room.name] = room.message_set.filter(
                    published__range=[speaker.last_connected,
                                      timezone.now()]).count()

            context = {
                'speaker': speaker,
                'rooms_list': rooms_list,
                'room_form': room_form,
                'total_messages_text': total_messages_text,
                'total_messages_image': total_messages_image,
                'active_rooms': active_rooms,
                'rooms_messages_since_last_connected': rooms_msg_since_last,
            }
            return render(request, 'decharla/main.html', context)
        else:  # Caso en el que se hace un POST del formulario de sala en Extra
            room_name = request.POST['name']
            try:
                room = Room.objects.get(name=room_name)
            except Room.DoesNotExist:
                # Creamos una nueva sala de charlas
                room = Room(name=room_name)
                room.save()
            return redirect('/' + str(room.name))
    else:
        return HttpResponseNotAllowed(['POST', 'GET'])

def logout_view(request):
    if request.method == 'GET':
        if 'cookie_id' in request.COOKIES:
            cookie_id = request.COOKIES['cookie_id']
        else:
            query_string = 'redir='
            redirect_url = '/login?' + query_string
            return redirect(redirect_url)
        speaker = Speaker.objects.get(cookie_id=cookie_id)
        speaker.delete()
        response = redirect('/login?redir=')
        response.delete_cookie('cookie_id')
        return response
    else:
        return HttpResponseNotAllowed(['POST', 'GET'])

def rooms(request, room_name):
    if (request.method == 'GET' and room_name != "favicon.ico") \
            or request.method == 'POST':
        if 'cookie_id' in request.COOKIES:
            cookie_id = request.COOKIES['cookie_id']
        else:
            query_string = 'redir=' + str(room_name)
            redirect_url = '/login?' + query_string
            return redirect(redirect_url)
        try:
            room = Room.objects.get(name=room_name)
        except Room.DoesNotExist:
            room = Room(name=room_name)
            room.save()
        speaker = Speaker.objects.get(cookie_id=cookie_id)
        if request.method == 'POST':
            action = request.POST['action']
            if action == 'room':
                room_name = request.POST['name']
                try:
                    room = Room.objects.get(name=room_name)
                except Room.DoesNotExist:
                    # Crear nueva sala
                    room = Room(name=room_name)
                    room.save()
                return redirect('/' + str(room.name))
            elif action == 'message':
                form = MessageForm(request.POST)
                if form.is_valid():
                    new_message = form.save(commit=False)
                    new_message.published = timezone.now()
                    new_message.speaker = speaker
                    new_message.room = room
                    new_message.save()
                    if new_message.isimage:
                        room.total_messages_image = room.total_messages_image \
                                                    + 1
                    else:
                        room.total_messages_text = room.total_messages_text \
                                                   + 1
                    room.save()
        rooms_list = Room.objects.all()
        total_messages_text, total_messages_image, \
            active_rooms = get_rooms_parameters(rooms_list)
        message_list = room.message_set.all().order_by('-published')
        message_form = MessageForm()
        room_form = RoomForm()
        context = {
            'room': room,
            'speaker': speaker,
            'message_form': message_form,
            'room_form': room_form,
            'message_list': message_list,
            'empty': room.is_empty(),
            'total_messages_text': total_messages_text,
            'total_messages_image': total_messages_image,
            'active_rooms': active_rooms
        }
        speaker.last_connected = timezone.now()
        speaker.save()
        return render(request, 'decharla/rooms.html', context)
    elif request.method != 'GET' or request.method != 'POST':  # Para que
        return HttpResponseNotAllowed(['POST', 'GET'])


def configuration(request):
    if request.method == 'GET' or request.method == 'POST':
        if 'cookie_id' in request.COOKIES:
            cookie_id = request.COOKIES['cookie_id']
        else:
            query_string = 'redir=configuracion'
            redirect_url = '/login?' + query_string
            return redirect(redirect_url)
        speaker = Speaker.objects.get(cookie_id=cookie_id)
        if request.method == 'POST':
            action = request.POST['action']
            if action == 'room':
                room_name = request.POST['name']
                try:
                    room = Room.objects.get(name=room_name)
                except Room.DoesNotExist:
                    room = Room(name=room_name)
                    room.save()
                speaker.last_connected = timezone.now()
                speaker.save()
                return redirect('/' + str(room.name))
            elif action == 'speakername':
                speaker.name = request.POST['name']
            elif action == 'style':
                speaker.font_size = request.POST['font_size']
                speaker.font_type = request.POST['font_type']
            speaker.save()
        room_form = RoomForm()
        style_form = StyleForm()
        speaker_form = SpeakerForm()
        rooms_list = Room.objects.all()
        total_messages_text, total_messages_image, \
            active_rooms = get_rooms_parameters(rooms_list)
        context = {
            'speaker': speaker,
            'room_form': room_form,
            'style_form': style_form,
            'speaker_form': speaker_form,
            'total_messages_text': total_messages_text,
            'total_messages_image': total_messages_image,
            'active_rooms': active_rooms
        }
        return render(request, 'decharla/configuration.html', context)
    else:
        return HttpResponseNotAllowed(['POST', 'GET'])


def help(request):
    if request.method == 'GET' or request.method == 'POST':
        if 'cookie_id' in request.COOKIES:
            cookie_id = request.COOKIES['cookie_id']
        else:
            query_string = 'redir=ayuda'
            redirect_url = '/login?' + query_string
            return redirect(redirect_url)
        speaker = Speaker.objects.get(cookie_id=cookie_id)
        if request.method == 'POST':
            action = request.POST['action']
            if action == 'room':
                room_name = request.POST['name']
                try:
                    room = Room.objects.get(name=room_name)
                except Room.DoesNotExist:
                    room = Room(name=room_name)
                    room.save()
                return redirect('/' + str(room.name))
        room_form = RoomForm()
        rooms_list = Room.objects.all()
        total_messages_text, total_messages_image, \
            active_rooms = get_rooms_parameters(rooms_list)
        context = {
            'speaker': speaker,
            'room_form': room_form,
            'total_messages_text': total_messages_text,
            'total_messages_image': total_messages_image,
            'active_rooms': active_rooms
        }
        return render(request, 'decharla/help.html', context)
    else:
        return HttpResponseNotAllowed(['POST', 'GET'])


def dynamic_rooms(request, room_name):
    if request.method == 'GET' or request.method == 'POST':
        if 'cookie_id' in request.COOKIES:
            cookie_id = request.COOKIES['cookie_id']
        else:
            query_string = 'redir=dinamica/' + str(room_name)
            redirect_url = '/login?' + query_string
            return redirect(redirect_url)
        try:
            room = Room.objects.get(name=room_name)
        except Room.DoesNotExist:
            room = Room(name=room_name)
            room.save()

        speaker = Speaker.objects.get(cookie_id=cookie_id)
        if request.method == 'POST':
            action = request.POST['action']
            if action == 'room':
                room_name = request.POST['name']
                try:
                    room = Room.objects.get(name=room_name)
                except Room.DoesNotExist:
                    room = Room(name=room_name)
                    room.save()
                return redirect('/' + str(room.name))
            elif action == 'message':
                form = MessageForm(request.POST)
                if form.is_valid():
                    new_message = form.save(commit=False)
                    new_message.published = timezone.now()
                    new_message.speaker = speaker
                    new_message.room = room
                    new_message.save()
                    if new_message.isimage:
                        room.total_messages_image = room.total_messages_image \
                                                    + 1
                    else:
                        room.total_messages_text = room.total_messages_text + 1
                    room.save()
        rooms_list = Room.objects.all()
        total_messages_text, total_messages_image, \
            active_rooms = get_rooms_parameters(rooms_list)
        message_form = MessageForm()
        room_form = RoomForm()
        context = {
            'room': room,
            'speaker': speaker,
            'message_form': message_form,
            'room_form': room_form,
            'empty': room.is_empty(),
            'total_messages_text': total_messages_text,
            'total_messages_image': total_messages_image,
            'active_rooms': active_rooms
        }
        speaker.last_connected = timezone.now()
        speaker.save()
        return render(request, 'decharla/dynrooms.html', context)
    else:
        return HttpResponseNotAllowed(['POST', 'GET'])

def json_room(request, room_name):
    if request.method == 'GET':
        if not ('cookie_id' in request.COOKIES):  # Si no hay cookie,
            query_string = 'redir=json/' + str(room_name)
            redirect_url = '/login?' + query_string
            return redirect(redirect_url)
        try:
            room = Room.objects.get(name=room_name)
        except Room.DoesNotExist:  # Si la sala no existe, se crea
            room = Room(name=room_name)
            room.save()
        message_list = room.message_set.all().order_by('-published')
        data = []
        for message in message_list:
            message_data = {
                "author": str(message.speaker.name),
                "text": message.content,
                "isimg": message.isimage,
                "date": message.published
            }
            data.append(message_data)
        return JsonResponse(data, safe=False, json_dumps_params={'indent': 4})
    else:
        return HttpResponseNotAllowed(['GET'])

@csrf_exempt
def update_rooms(request, room_name):
    if request.method == 'PUT':
        authorization_header = request.META.get('HTTP_AUTHORIZATION')
        if 'cookie_id' in request.COOKIES:
            cookie_id = request.COOKIES['cookie_id']
            speaker = Speaker.objects.get(cookie_id=cookie_id)
        else:
            try:
                AccessKeys.objects.get(key=authorization_header)
                cookie_id = random.randint(1, 2 ** 32)
                last_connected = timezone.now()
                speaker = Speaker(cookie_id=cookie_id,
                                  last_connected=last_connected)
                speaker.save()
            except AccessKeys.DoesNotExist:
                form = AuthenticateForm()
                context = {
                    'form': form,
                    'key_not_valid': True,
                }
                response = render(request,
                                  'decharla/authenticate.html', context)
                response.status_code = 401
                return response
        try:
            room = Room.objects.get(name=room_name)
        except Room.DoesNotExist:
            room = Room(name=room_name)
            room.save()
        parser = make_parser()
        handler = RoomUpdateHandler(room, speaker)
        parser.setContentHandler(handler)
        parser.parse(request)
        handler.update_room()
        return redirect('/' + str(room.name))
    else:
        return HttpResponseNotAllowed(['PUT'])

