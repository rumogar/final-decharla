from django import forms
from .models import Room, Speaker, Message, AccessKeys

# formulario para crear salas

class RoomForm(forms.ModelForm):
    class Meta:
        model = Room
        fields = ('name',)

class SpeakerForm(forms.ModelForm):
    class Meta:
        model = Speaker
        fields = ('name',)

class StyleForm(forms.ModelForm):
    class Meta:
        model = Speaker
        fields = ('font_size', 'font_type')

class MessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ('content', 'isimage')

class AuthenticateForm(forms.ModelForm):
    class Meta:
        model = AccessKeys
        fields = ('key', )