from xml.sax.handler import ContentHandler
from .models import Message
from django.utils import timezone


class RoomUpdateHandler(ContentHandler):
    def __init__(self, room, speaker):
        self.room = room
        self.speaker = speaker
        self.current_message = None  # diccionario con la información del mensaje que se está leyendo
        self.messages = []
        self.inContent = False
        self.theContent = ""

    def startElement(self, name, attrs):
        if name == 'message':
            self.current_message = {'isimg': attrs.get('isimg')}
        elif name == 'text':
            self.inContent = True

    def endElement(self, name):
        if name == "message":
            self.messages.append(self.current_message)
            self.current_message = None
        elif name == 'text':
            self.current_message['text'] = self.theContent
        if self.inContent:
            self.inContent = False
            self.theContent = ""

    def characters(self, chars):
        if self.inContent:
            self.theContent = self.theContent + chars

    def update_room(self):
        for message in self.messages:
            is_image = message.get('isimg') == "true"
            content = message.get('text')
            message = Message(content=content, isimage=is_image, published=timezone.now(), speaker=self.speaker,
                              room=self.room)
            message.save()
        self.room.total_messages_text = self.room.message_set.filter(isimage=False).count()
        self.room.total_messages_image = self.room.message_set.filter(isimage=True).count()
        self.room.save()
