# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Rubén Morán García
* Titulación: ITT
* Cuenta en laboratorios: rumogar
* Cuenta URJC: r.moran.2019
* Video básico (url): https://youtu.be/BJwYt8kY1gg
* Video parte opcional (url): https://youtu.be/f47XOu2ECjA
* Despliegue (url): http://rubenmoran52.pythonanywhere.com/
* Contraseñas: saro2223, st2233
* Cuenta Admin Site: rumogar/contraseña -> sat2223

## Resumen parte obligatoria
Al comenzar, la aplicación requerirá que el usuario proporcione una contraseña de acceso para poder ingresar al sitio. Una vez que el usuario ha ingresado con éxito utilizando una contraseña válida, no será necesario autenticarse nuevamente, ya que se almacenará una cookie de sesión. En todas las páginas de la aplicación, se mostrará una cabecera consistente, un menú que brinda acceso a diferentes recursos, un formulario para ingresar a una sala (ya sea existente o nueva) y un pie de página que muestra estadísticas relacionadas con las salas.

En la página principal, se mostrará un listado completo de todas las salas creadas. Para cada sala, se presentarán los mensajes existentes, así como aquellos mensajes que se hayan agregado desde la última visita del usuario. La página de cada sala permitirá a los usuarios comentar textos o imágenes, y estos comentarios se mostrarán debajo del formulario, ordenados de más reciente a menos reciente. Además, habrá un botón para acceder a la página en formato JSON, que ofrecerá información de la sala de manera estructurada.

La página de configuración brindará la opción de cambiar el nombre del usuario que participa en las conversaciones, así como personalizar el estilo y el tamaño de la fuente utilizada para mostrar los párrafos y mensajes.

En la página de ayuda, se proporcionarán detalles sobre la autoría de la aplicación y una breve descripción de cómo funciona. Además, se explicará la existencia de un recurso especial de sala dinámica, donde los mensajes se actualizarán automáticamente mediante el uso de un script de JavaScript. También se mencionará la disponibilidad de un recurso adicional, que permitirá a los usuarios enviar un archivo XML a través de una solicitud PUT para actualizar los mensajes de una sala específica. Cabe destacar que esta última funcionalidad no estará disponible en la versión de despliegue debido a un error 403, pero funcionará correctamente en una ejecución local al utilizar csrf-exempt.
## Lista partes opcionales

* Incorporación del favicon.ico: Se ha incluido un ícono personalizado para que se muestre en los navegadores como un símbolo distintivo de la aplicación.
* Implementación de función de cierre de sesión: Se ha agregado una función de cierre de sesión que permite al usuario desconectarse y redirigirse nuevamente a la página de inicio de sesión.
* Mejora en el script de Javascript: Se han realizado modificaciones en el script para que los mensajes en las salas dinámicas se muestren con un formato similar al de las salas originales, proporcionando una experiencia visual coherente.
* Representación de la web mediante pythonanywhere: Se ha desplegado la aplicación en pythonanywhere, y se ha configurado para que se muestre correctamente en la web.